# AppIonic4

## Environment

Angular CLI: 8.0.6
Ionic Framework: @ionic/angular 4.2.0
Node: 10.15.3
OS: win32 x64
Angular: 8.0.3

# Initialize Front End App
    1. Clone this repo to your local machine run `git clone https://gitlab.com/csaramos92/angularcrud.git`
    2. Run --> `npm install`
    3. Run --> `ionic serve`