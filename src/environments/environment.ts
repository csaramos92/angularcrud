// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyAAL4pGq8S835HRDZFI60s-DcfAk0h6q8w",
    authDomain: "angularcrud-f32a0.firebaseapp.com",
    databaseURL: "https://angularcrud-f32a0.firebaseio.com",
    projectId: "angularcrud-f32a0",
    storageBucket: "angularcrud-f32a0.appspot.com",
    messagingSenderId: "129900905642",
    appId: "1:129900905642:web:129316305048499e9a3cd3"
  }
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
