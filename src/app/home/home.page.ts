import { Component, OnInit } from '@angular/core';
import { RestApiService } from '../services/rest-api.service'
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  
 jsonDataFromUrl;
 originalDates;
 searchTerm;

  constructor(
    private restApiService: RestApiService,
    private loadingController: LoadingController
    ) { }

  ngOnInit() {
    const apiUrl = 'https://jsonplaceholder.typicode.com/todos';
    this.loadData(apiUrl);  
  }

  async loadData(apiUrl){
    const loading = await this.loadingController.create({
      message:'Cargando...'
    });
    await loading.present();
    this.restApiService.getDataFromUrl(apiUrl).subscribe(data=> {
      loading.dismiss();
      this.jsonDataFromUrl = data;
      this.originalDates = data;
    },
    err=>{
      loading.dismiss();
    });
  }

  setFilteredItems(clear) {
    this.jsonDataFromUrl = this.originalDates;
    var val = this.searchTerm;

    if (val && val.trim() != '' && !clear) {
      this.jsonDataFromUrl = this.jsonDataFromUrl.filter((item) => {
        return (item.title.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    } else {
      this.jsonDataFromUrl = this.originalDates;
    }
  }
  
}
