import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: './home/home.module#HomePageModule'
  },
  { 
    path: 'view-list',
    loadChildren: './pages/view-list/view-list.module#ViewListPageModule'
  },
  {
    path: 'view-todos', 
    loadChildren: './pages/view-todos/view-todos.module#ViewTodosPageModule' 
  },
  { 
    path: 'delete-item', 
    loadChildren: './pages/delete-item/delete-item.module#DeleteItemPageModule' 
  } , 
  {
    path: 'edit-item/:id', 
    loadChildren: './pages/edit-item/edit-item.module#EditItemPageModule' 
  },
  { 
    path: 'edit-item', 
    loadChildren: './pages/edit-item/edit-item.module#EditItemPageModule' 
  },
  { 
    path: 'create-item', 
    loadChildren: './pages/create-item/create-item.module#CreateItemPageModule' 
  }

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
