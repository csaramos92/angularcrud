import { Component, OnInit } from '@angular/core';
import { DataI } from '../../models/data.interfacce';
import { DataBaseService } from '../../services/data-base.service';
import { ActivatedRoute } from '@angular/router';
import { NavController, LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-create-item',
  templateUrl: './create-item.page.html',
  styleUrls: ['./create-item.page.scss'],
})
export class CreateItemPage implements OnInit {

  userData: DataI = {
    title: '',
    userId: null
  };
  
  constructor(
    private route: ActivatedRoute, private nav:NavController, 
    private dataBaseService: DataBaseService, private loadingController: LoadingController
  ) {  }

  ngOnInit() {
  }

  async saveData(){
    const loading = await this.loadingController.create({
      message:'Creando...'
    });
    await loading.present();
      this.dataBaseService.addNewData(this.userData).then(() =>{
        loading.dismiss();
        this.nav.navigateForward('view-todos');
      })
  }

}


