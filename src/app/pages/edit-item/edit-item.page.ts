import { Component, OnInit } from '@angular/core';
import { DataI } from '../../models/data.interfacce';
import { DataBaseService } from '../../services/data-base.service';
import { ActivatedRoute } from '@angular/router';
import { NavController, LoadingController, AlertController  } from '@ionic/angular';

@Component({
  selector: 'app-edit-item',
  templateUrl: './edit-item.page.html',
  styleUrls: ['./edit-item.page.scss'],
})
export class EditItemPage implements OnInit {

  dataFromServer: DataI = {
    title: '',
    userId: 0
  };
  dataId = null;
  
  constructor(
    private route: ActivatedRoute, private nav:NavController, 
    private dataBaseService: DataBaseService, private loadingController: LoadingController,
    public alertController: AlertController
  ) {  }

  async presentAlertConfirm(header, message, ) {
    const alert = await this.alertController.create({
      header: header,
      message: message,
      buttons: [
        {
          text: 'Okay',
          handler: () => {
            this.nav.navigateForward('view-todos');
          }
        }
      ]
    });

    await alert.present();
  }

  ngOnInit() {
    this.dataId = this.route.snapshot.params['id'];
    if(this.dataId) {
      this.loadData();
    }
  }

  async loadData(){
    const loading = await this.loadingController.create({
      message:'Cargando...'
    });
    await loading.present();
    this.dataBaseService.getDataById(this.dataId).subscribe(data => {
      loading.dismiss();
      this.dataFromServer = data;
    });
  }

  async saveData(){
    const loading = await this.loadingController.create({
      message:'Actualizando...'
    });
    await loading.present();
      this.dataBaseService.updateDataById(this.dataFromServer, this.dataId).then(() =>{

        loading.dismiss();
        const header = 'Datos actualizados';
        const message = '<strong>Los datos han sido actualizados con exito</strong>';
        this.presentAlertConfirm(header,message);
      })
      .catch(()=>{
        loading.dismiss();
        const header = 'Lo sentimos';
        const message = '<strong>Los datos no se han podido eliminar, intente mas tarde</strong>';
        this.presentAlertConfirm(header,message); 
    });
  }

  async removeData(){
    const loading = await this.loadingController.create({
      message:'Eliminando...'
    });
    await loading.present();
    if(this.dataId){
      this.dataBaseService.removeDataById(this.dataId).then(() =>{
        loading.dismiss();
        const header = 'Datos eliminados';
        const message = '<strong>Los datos han sido eliminados con exito</strong>';
        this.presentAlertConfirm(header,message);  
      })
      .catch(()=>{
        loading.dismiss();
        const header = 'Lo sentimos';
        const message = '<strong>Los datos no se han podido eliminar, intente mas tarde</strong>';
        this.presentAlertConfirm(header,message);  
      })
    }
  }

}

