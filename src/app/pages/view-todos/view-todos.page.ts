import { Component, OnInit } from '@angular/core';
import { DataI } from '../../models/data.interfacce';
import { DataBaseService } from '../../services/data-base.service';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-view-todos',
  templateUrl: './view-todos.page.html',
  styleUrls: ['./view-todos.page.scss'],
})
export class ViewTodosPage implements OnInit {

  dataFromServer: DataI[];
  
  constructor(
    private dataBaseService: DataBaseService,
    private loadingController: LoadingController
    ) { }

  ngOnInit() {
    this.loadDataFromServer();
  }

  async loadDataFromServer() {
    const loading = await this.loadingController.create({
      message:'Cargando...'
    });
    await loading.present();
    this.dataBaseService.getDataFromServer().subscribe(data => {
      loading.dismiss();
      this.dataFromServer = data;
    });
  }
}
