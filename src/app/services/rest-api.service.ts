import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RestApiService {

  constructor(private http: HttpClient) { }

  url = 'https://jsonplaceholder.typicode.com/todos';

  getDataFromUrl(url) {
    return this.http.get(url);
  }
}