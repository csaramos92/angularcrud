import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable, from } from 'rxjs';
import { map } from 'rxjs/operators';
import { DataI } from '../models/data.interfacce';

@Injectable({
  providedIn: 'root'
})
export class DataBaseService {
  private dataBaseCollection: AngularFirestoreCollection<DataI>;
  private dataFromServer: Observable<DataI[]>;

  constructor(db: AngularFirestore) {
    this.dataBaseCollection = db.collection<DataI>('dates');
    this.dataFromServer = this.dataBaseCollection.snapshotChanges().pipe(map(
      actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return {id, ...data };
        });
      }
    ));
   }

   getDataFromServer() {
    return this.dataFromServer;
   }

   getDataById(id:string){
     return this.dataBaseCollection.doc<DataI>(id).valueChanges();
   }

   updateDataById(newData: DataI, id: string){
     return this.dataBaseCollection.doc(id).update(newData);
   }

   addNewData(newData: DataI) {
     return this.dataBaseCollection.add(newData);
   }

   removeDataById(id: string){
     return this.dataBaseCollection.doc(id).delete();
   }
}
