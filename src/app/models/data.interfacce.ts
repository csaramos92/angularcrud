export interface DataI {
    id?: string;
    title: string;
    userId: number;
}